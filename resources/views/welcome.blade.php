<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <link rel="stylesheet" href="css/app.css">
    </head>

    <body>
        <div id="app" class="container">
            <revenue-graph url="/api/revenue"></revenue-graph>
        </div>

        <script src="js/app.js"></script>
    </body>
</html>
