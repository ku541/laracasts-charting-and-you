<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Performance;
use Faker\Generator as Faker;

$factory->define(Performance::class, function (Faker $faker) {
    return [
        'revenue' => $faker->randomFloat(2, 0),
        'new_users' => $faker->numberBetween(0),
        'users' => $faker->numberBetween(0),
        'created_at' => $faker->dateTimeBetween('-30 days', '+0 days')
    ];
});
